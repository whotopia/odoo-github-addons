#!/bin/bash 

git clone -b 15.0 https://github.com/OCA/account-analytic.git
git clone -b 15.0 https://github.com/OCA/account-budgeting.git
git clone -b 15.0 https://github.com/OCA/account-closing.git
git clone -b 15.0 https://github.com/OCA/account-consolidation.git
git clone -b 15.0 https://github.com/OCA/account-financial-reporting.git
git clone -b 15.0 https://github.com/OCA/account-financial-tools.git
git clone -b 15.0 https://github.com/OCA/account-fiscal-rule.git
git clone -b 15.0 https://github.com/OCA/account-invoice-reporting.git
git clone -b 15.0 https://github.com/OCA/account-invoicing.git
git clone -b 15.0 https://github.com/OCA/account-payment.git
git clone -b 15.0 https://github.com/OCA/account-reconcile.git
git clone -b 15.0 https://github.com/guohuadeng/app-odoo.git guohuadeng_app-odoo
git clone -b 15.0 https://github.com/OCA/bank-payment.git
git clone -b 15.0 https://github.com/OCA/bank-statement-import.git
git clone -b 15.0 https://github.com/OCA/brand.git
git clone -b 15.0 https://github.com/OCA/business-requirement.git
git clone -b 15.0 https://github.com/OCA/calendar.git
git clone -b 15.0 https://github.com/OCA/community-data-files.git
git clone -b 15.0 https://github.com/OCA/connector.git
git clone -b 15.0 https://github.com/OCA/connector-ecommerce.git
git clone -b 15.0 https://github.com/OCA/connector-interfaces.git
git clone -b 15.0 https://github.com/OCA/connector-lims.git
git clone -b 15.0 https://github.com/OCA/connector-odoo2odoo.git
git clone -b 15.0 https://github.com/OCA/connector-telephony.git
git clone -b 15.0 https://github.com/OCA/connector-redmine.git
git clone -b 15.0 https://github.com/OCA/connector-lims.git
git clone -b 15.0 https://github.com/OCA/contract.git
git clone -b 15.0 https://github.com/OCA/credit-control.git
git clone -b 15.0 https://github.com/OCA/crm.git
git clone -b 15.0 https://github.com/OCA/currency.git
git clone -b 15.0 https://github.com/CybroOdoo/CybroAddons.git CybroOdoo_CybroAddons
git clone -b 15.0 https://github.com/OCA/data-protection.git
git clone -b 15.0 https://github.com/OCA/ddmrp.git
git clone -b 15.0 https://github.com/OCA/delivery-carrier,git
git clone -b 15.0 https://github.com/OCA/donation.git
git clone -b 15.0 https://github.com/OCA/dms.git
git clone -b 15.0 https://github.com/OCA/e-commerce.git
git clone -b 15.0 https://github.com/OCA/edi.git
git clone -b 15.0 https://github.com/OCA/event.git
git clone -b 15.0 https://github.com/OCA/field-service.git
git clone -b 15.0 https://github.com/OCA/fleet.git
git clone -b 15.0 https://github.com/OCA/geospatial.git
git clone -b 15.0 https://github.com/OCA/helpdesk.git
git clone -b 15.0 https://github.com/OCA/hr-expense.git
git clone -b 15.0 https://github.com/OCA/hr-holidays.git
git clone -b 15.0 https://github.com/OCA/hr.git
git clone -b 15.0 https://github.com/OCA/intrastat-extrastat.git
git clone -b 15.0 https://github.com/OCA/infrastructure.git
git clone -b 15.0 https://github.com/OCA/interface-github.git
git clone -b 15.0 https://github.com/OCA/iot.git
git clone -b 15.0 https://github.com/OCA/knowledge.git
git clone -b 15.0 https://github.com/OCA/l10n-ethiopia.git
git clone -b 15.0 https://github.com/OCA/l10n-germany.git
git clone -b 15.0 https://github.com/OCA/l10n-morocco.git
git clone -b 15.0 https://github.com/OCA/l10n-france.git
git clone -b 15.0 https://github.com/OCA/l10n-italy.git
git clone -b 15.0 https://github.com/OCA/l10n-switzerland.git
git clone -b 15.0 https://github.com/yvaucher/l10n-switzerland.git yvaucher_l10n-switzerland
git clone -b 15.0 https://github.com/OCA/l10n-vietnam.git
git clone -b 15.0 https://github.com/OCA/l10n-usa.git
git clone -b 15.0 https://github.com/OCA/l10n-united-kingdom.git
git clone -b 15.0 https://github.com/OCA/maintenance.git
git clone -b 15.0 https://github.com/OCA/management-system.git
git clone -b 15.0 https://github.com/OCA/manufacture.git
git clone -b 15.0 https://github.com/OCA/manufacture-reporting.git
git clone -b 15.0 https://github.com/OCA/margin-analysis.git
git clone -b 15.0 https://github.com/OCA/mis-builder-contrib.git
git clone -b 15.0 https://github.com/OCA/mis-builder.git
git clone -b 15.0 https://github.com/OCA/multi-company.git
git clone -b 15.0 https://github.com/novacode-nl/odoo-formio.git novacode-nl_odoo-formio
git clone -b 15.0 https://github.com/odoomates/odooapps.git odoomates_odooapps
git clone -b 15.0 https://github.com/novacode-nl/odoo-addons.git
git clone -b 15.0 https://github.com/OCA/odoo-pim.git
git clone -b 15.0 https://github.com/OCA/odoorpc.git
git clone -b 15.0 https://github.com/OCA/operating-unit.git
git clone -b 15.0 https://github.com/OCA/partner-contact.git
git clone -b 15.0 https://github.com/OCA/payroll.git
git clone -b 15.0 https://github.com/OCA/pms.git
git clone -b 15.0 https://github.com/OCA/project.git
git clone -b 15.0 https://github.com/OCA/product-attribute.git
git clone -b 15.0 https://github.com/OCA/product-configurator.git
git clone -b 15.0 https://github.com/OCA/product-pack.git
git clone -b 15.0 https://github.com/OCA/product-variant.git
git clone -b 15.0 https://github.com/OCA/queue.git
git clone -b 15.0 https://github.com/OCA/intrastat-extrastat.git
git clone -b 15.0 https://github.com/OCA/payroll.git
git clone -b 15.0 https://github.com/OCA/pos.git
git clone -b 15.0 https://github.com/OCA/product-pack.git
git clone -b 15.0 https://github.com/OCA/product-kitting.git
git clone -b 15.0 https://github.com/OCA/program.git
git clone -b 15.0 https://github.com/OCA/project-agile.git
git clone -b 15.0 https://github.com/OCA/project.git
git clone -b 15.0 https://github.com/OCA/project-reporting.git
git clone -b 15.0 https://github.com/OCA/purchase-reporting.git
git clone -b 15.0 https://github.com/OCA/purchase-workflow.git
git clone -b 15.0 https://github.com/OCA/reporting-engine.git
git clone -b 15.0 https://github.com/OCA/report-print-send.git
git clone -b 15.0 https://github.com/OCA/rest-framework.git
git clone -b 15.0 https://github.com/OCA/repair.git
git clone -b 15.0 https://github.com/OCA/rma.git
git clone -b 15.0 https://github.com/OCA/sale-financial.git
git clone -b 15.0 https://github.com/OCA/sale-promotion.git
git clone -b 15.0 https://github.com/OCA/sale-reporting.git
git clone -b 15.0 https://github.com/OCA/sale-workflow.git
git clone -b 15.0 https://github.com/OCA/search-engine.git
git clone -b 15.0 https://github.com/OCA/server-auth.git
git clone -b 15.0 https://github.com/OCA/server-backend.git
git clone -b 15.0 https://github.com/OCA/server-brand.git
git clone -b 15.0 https://github.com/OCA/server-env.git
git clone -b 15.0 https://github.com/OCA/server-tools.git
git clone -b 15.0 https://github.com/OCA/server-ux.git
git clone -b 15.0 https://github.com/Smile-SA/odoo_addons.git Smile-SA_odoo_addons
git clone -b 15.0 https://github.com/smottet/odoo_addons.git smottet_odoo_addons

git clone -b 15.0 https://github.com/OCA/social.git
git clone -b 15.0 https://github.com/OCA/stock-logistics-barcode.git
git clone -b 15.0 https://github.com/OCA/stock-logistics-reporting.git
git clone -b 15.0 https://github.com/OCA/stock-logistics-tracking.git
git clone -b 15.0 https://github.com/OCA/stock-logistics-transport.git
git clone -b 15.0 https://github.com/OCA/stock-logistics-warehouse.git
git clone -b 15.0 https://github.com/OCA/stock-logistics-workflow.git
git clone -b 15.0 https://github.com/OCA/shift-planning.git
git clone -b 15.0 https://github.com/OCA/storage.git
git clone -b 15.0 https://github.com/OCA/survey.git
git clone -b 15.0 https://github.com/OCA/timesheet.git
git clone -b 15.0 https://github.com/OCA/vertical-agriculture.git
git clone -b 15.0 https://github.com/OCA/vertical-association.git
git clone -b 15.0 https://github.com/OCA/vertical-community.git
git clone -b 15.0 https://github.com/OCA/vertical-construction.git
git clone -b 15.0 https://github.com/OCA/vertical-edition.git
git clone -b 15.0 https://github.com/OCA/vertical-education.git
git clone -b 15.0 https://github.com/OCA/vertical-hotel.git
git clone -b 15.0 https://github.com/OCA/vertical-isp.git
git clone -b 15.0 https://github.com/OCA/vertical-medical.git
git clone -b 15.0 https://github.com/OCA/vertical-ngo.git
git clone -b 15.0 https://github.com/OCA/vertical-rental
git clone -b 15.0 https://github.com/OCA/vertical-travel
git clone -b 15.0 https://github.com/OCA/vertical-realestate
git clone -b 15.0 https://github.com/OCA/web.git
git clone -b 15.0 https://github.com/OCA/website-cms.git
git clone -b 15.0 https://github.com/OCA/website.git
git clone -b 15.0 https://github.com/OCA/website-themes.git
git clone -b 15.0 https://github.com/OCA/wms.git

git clone -b 15.0 https://github.com/Yenthe666/auto_backup.git Yenthe666_auto_backup
git clone -b 15.0 https://github.com/YvanDotet/print_contact.git YvanDotet_print_contact
git clone -b 15.0 https://github.com/YvanDotet/query_deluxe.git YvanDotet_query_deluxe
git clone -b 15.0 https://github.com/gityopie/odoo-addons gityopie_odoo-addons
